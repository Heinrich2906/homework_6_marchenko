package com.company;

/**
 * Created by heinr on 19.10.2016.
 */
class Cow extends Pet{

    Cow(String name){
        this.name = name;
        this.type = Main.type.MAMMAL;
    }

    @Override
    public void voice(){
        System.out.println("I'm " + this.name + ". My name's " + this.name + ". My voice is muu");
    }
}
