package com.company;

/**
 * Created by heinr on 19.10.2016.
 */
public class Dog extends Pet{

    Dog(String name){
        this.name = name;
        this.type = Main.type.MAMMAL;
    }

    @Override
    public void voice(){
        System.out.println("I'm " + this.name + ". My name's " + this.name + ". My voice is wow");
    }
}
