package com.company;

/**
 * Created by heinr on 19.10.2016.
 */
public class Main {

    enum type {MAMMAL, AMPHIBIAN, BIRD, FISH}

    ;

    public static void main(String[] args) {

        Cow cow = new Cow("Zor'ka");
        cow.voice();

        Cat cat = new Cat("Waska");
        cat.voice();

        Dog hund = new Dog("Bobick");
        hund.voice();
    }


}

