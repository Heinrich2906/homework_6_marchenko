package com.part2;

/**
 * Created by heinr on 19.10.2016.
 */
abstract public class Animal {

    protected String name;
    protected byte countOfLegs;
    protected byte countOfWings;
    protected String colour;
    protected Main.type type;

    public abstract void voice();

}

interface swimable{
    String swima();
}

interface rubable{
    String run();
}

interface flyable{
    String fly();
}
