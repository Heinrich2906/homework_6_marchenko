package com.part2;

/**
 * Created by heinr on 19.10.2016.
 */
public class Cat extends Animal {

    byte countOfWings = 0;
    byte countOfLegs = 4;

    Cat(String name, String colour){
        this.colour = colour;
        this.name = name;
    }

    @Override
    public void voice(){
        System.out.print("My voice's myau");
    }
}
