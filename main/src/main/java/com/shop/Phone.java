package com.shop;

/**
 * Created by heinr on 23.10.2016.
 */
public class Phone {

    private String producer;    // бренд
    private String model;       // модель
    private int numberOfSim; // количество сим-карт
    private boolean hasCamera;   // имеет ли камеру
    private String color;       // цвет
    private BodyType.PhoneType phoneType; // тип корпуса

    Phone(String producer, String model, int numberOfSim, boolean hasCamera, String color, BodyType.PhoneType phoneType) {

        if (numberOfSim == 0) this.numberOfSim = 1;
        else this.numberOfSim = numberOfSim;
        this.phoneType = phoneType;
        this.hasCamera = hasCamera;
        this.producer = producer;
        this.model = model;
        this.color = color;

    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setHasCamera(boolean hasCamera) {
        this.hasCamera = hasCamera;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setNumberOfSim(byte numberOfSim) {
        this.numberOfSim = numberOfSim;
    }

    public void setPhoneType(BodyType.PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Phone phone = (Phone) obj;

        boolean result;

        result = false;

        if (this.numberOfSim == phone.numberOfSim
                & this.hasCamera == phone.hasCamera
                & this.model.equals(phone.model)
                & this.phoneType.equals(phone.phoneType)
                & this.producer.equals(phone.producer)) result = true;

        return result;
    }

    @Override
    public int hashCode() {

        int result;

        if (hasCamera == true) result = 29;
        else result = 13;

        result = 29 * result + phoneType.hashCode();
        result = 29 * result + producer.hashCode();
        result = 29 * result + model.hashCode();
        result = 29 * result + numberOfSim;

        return result;
    }

    @Override
    public String toString() {

        String result;

        result = producer + " " + model + ", " + phoneType + ", цвет " + color + ", камера "
                + hasCamera + ", количество SIM карт " + numberOfSim;

        return result;
    }
}
