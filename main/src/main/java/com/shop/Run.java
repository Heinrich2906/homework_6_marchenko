package com.shop;

/**
 * Created by heinr on 23.10.2016.
 */
public class Run {

    public static void main(String[] args) throws Exception{

        Shop shop = new Shop();

        Phone neo = new Phone("Sony", "Neo", 1, true,"black", BodyType.PhoneType.SENSOR);
        Phone a5 = new Phone("Samsung", "A5", 1, true,"white", BodyType.PhoneType.SENSOR);
        Phone aliut = new Phone("Nokia", "3310", 1, true,"blue", BodyType.PhoneType.BLOCK);

        Phone[] prichod = new Phone[2];
        prichod[0] = a5;
        prichod[1] = neo;

        shop.newDelivery(prichod);

        shop.printPhonesInStock();

        boolean result;

        result = shop.searchPhoneInStock(neo);

        System.out.println(neo.toString() + ", найден " + result);

        result = shop.searchPhoneInStock(aliut);

        System.out.println(aliut.toString() + ", найден " + result);

    }
}
