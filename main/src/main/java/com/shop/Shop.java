package com.shop;

/**
 * Created by heinr on 23.10.2016.
 */
public class Shop {

    Phone[] phonesInStock;

    public Phone[] getPhonesInStock() {
        return phonesInStock;
    }

    public boolean searchPhoneInStock(Phone searchPhone) {

        boolean result;

        result = false;

        for (Phone s : this.phonesInStock) {
            if (s.equals(searchPhone)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public void newDelivery(Phone[] phonesDelivery) {

        int arraySise;

        if (phonesDelivery == null | phonesDelivery.length == 0) return;
        if (phonesInStock == null) {

            arraySise = phonesDelivery.length;
            this.phonesInStock = new Phone[arraySise];

            for (int i = 0; i < arraySise; i++) {phonesInStock[i] = phonesDelivery[i];}

        } else {

            arraySise = phonesDelivery.length + phonesInStock.length;

            Phone[] newArray = new Phone[arraySise];

            for (int i = 0; i < phonesInStock.length; i++) {newArray[i] = phonesInStock[i];}

            this.phonesInStock = new Phone[arraySise];

            for (int i = 0; i < newArray.length; i++) {phonesInStock[i] = newArray[i];}
            for (int i = newArray.length; i < arraySise; i++) {phonesInStock[i] = phonesDelivery[i - newArray.length];}

        }
    }

    public int countOfSearchPhone(Phone searchPhone) {

        int result = 0;

        for (Phone s : this.phonesInStock) {
            if (s.equals(searchPhone)) ++result;
        }

        return result;
    }

    public void printPhonesInStock() {
        for (Phone s : phonesInStock) {
            System.out.println(s.toString());
        }
    }
}
